<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.linkedin.com/in/sahed-sawon-8629a978?trk=hp-identity-name
 * @since      1.0.0
 *
 * @package    Title_tag_and_meta_description_length_tool
 * @subpackage Title_tag_and_meta_description_length_tool/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
