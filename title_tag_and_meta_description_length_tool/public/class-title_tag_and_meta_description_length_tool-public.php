<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.linkedin.com/in/sahed-sawon-8629a978?trk=hp-identity-name
 * @since      1.0.0
 *
 * @package    Title_tag_and_meta_description_length_tool
 * @subpackage Title_tag_and_meta_description_length_tool/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Title_tag_and_meta_description_length_tool
 * @subpackage Title_tag_and_meta_description_length_tool/public
 * @author     sahed sawon <smsawoncse@gmail.com>
 */
class Title_tag_and_meta_description_length_tool_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Title_tag_and_meta_description_length_tool_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Title_tag_and_meta_description_length_tool_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/title_tag_and_meta_description_length_tool-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'smsboot', plugin_dir_url( __FILE__ ) . 'css/sms.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Title_tag_and_meta_description_length_tool_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Title_tag_and_meta_description_length_tool_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/title_tag_and_meta_description_length_tool-public.js', array( 'jquery' ), $this->version, false );

	}

}
