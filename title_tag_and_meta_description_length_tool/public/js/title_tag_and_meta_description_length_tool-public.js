(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	 $(function(){

	 	//Title Tag jquery
	 	$("#tTitleTag").on("paste",function(e){
	 		var pastedData = e.originalEvent.clipboardData.getData('text').length;
    		$("#tTitleTagResults").text(pastedData);
	 	});
	 	$("#tTitleTag").on("keyup",function(e){
	 		var tTitleTagResults = '';
    			var character = parseInt($("#tTitleTag").val().length);
    			if (character < 25 ) {
    				tTitleTagResults = '<h4>'+character+' characters is pretty short. We recommend a <strong style="color:red">minimum</strong> of 25 characters.</h4>';
    			}else if (character > 65 ) {
    				tTitleTagResults = '<h4><span class="text-danger"> Characters:'+character+'/65 reccomended maximum.</span> We recommend a maximum of 65 characters.</h4>';
    			}else{
    				tTitleTagResults = '<h4 class="text-success">Characters:'+character+'/65 reccomended maximum.</h4>';
    			}
    			$("#tTitleTagResults").html(tTitleTagResults);
	 	});
	 

	 	//Meta Description jquery
	 	$("#tMetaTag").on("paste",function(e){
	 		var pastedData = e.originalEvent.clipboardData.getData('text').length;
    		$("#tMetaTagResults").text(pastedData);
	 	});
	 	$("#tMetaTag").on("keyup",function(e){
	 			var tMetaTagResults = '';
    			var character = parseInt($("#tMetaTag").val().length);
    			if (character < 100 ) {
    				tMetaTagResults = '<h4>'+character+' characters is pretty short. We recommend a <strong>minimum</strong> of 100 characters.</h4>';
    			}else if (character > 155 ) {
    				tMetaTagResults = '<h4><span class="text-danger"> Characters:'+character+'/155 reccomended maximum.</span> We recommend a maximum of 155 characters.</h4>';
    			}else{
    				tMetaTagResults = '<h4 class="text-success">Characters:'+character+'/155 reccomended maximum.</h4>';
    			}
    			$("#tMetaTagResults").html(tMetaTagResults);
	 	});

	 	//Character Counter jquery
	 	$("#tGeneral").on("paste",function(e){
	 		var pastedData = e.originalEvent.clipboardData.getData('text').length;
    		$("#tGeneralResults").text(pastedData);
	 	});
	 	$("#tGeneral").on("keyup",function(e){
    			var character3 = $("#tGeneral").val().length;
    			var tGeneralResults = '<h4>Characters: <strong>'+character3+'</strong></h4>';
    			$("#tGeneralResults").html(tGeneralResults);
	 	});
	 });
	 

})( jQuery );
