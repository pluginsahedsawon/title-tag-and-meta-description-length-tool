=== Plugin Name ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: https://www.linkedin.com/in/sahed-sawon-8629a978?trk=hp-identity-name
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Title tag and meta description length tool is a small plugin for meta title tag and meta description tag length check.

== Description ==

How many characters can I have in my title and meta description? How many characters is this!? Save a few seconds and paste your title tag here and check the length of your character string.

<title> Tag

Best practices for title tag length say that your title should no more than 60-80 characters. An important number to keep in mind is Google's maximum title threshold of around 65 characters. Anything longer and your title starts to get truncated.

Meta Description Tag

Meta descriptions can be a bit longer, but Google tends to show roughly 155 (two lines) characters when it chooses to use your meta description on result pages. 


*   "Contributors" smsawoncse
*   tags: html,css,javascript,jquery,wordpress,php, SEO, 
*   Requires: 4.5 or higher
*   Compatible up to: 4.6.1


== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `title_tag_and_meta_description_length_tool.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
2. Click to add new submenu under page menu.
3. Choose Title Tag and Meta Description Length Tool template from right side of admin page under template portion.  
4. click to publish button. 
5. view page 
  Everything done. now you are ready to use Title Tag and Meta Description Length Tool plugin on your blog.


