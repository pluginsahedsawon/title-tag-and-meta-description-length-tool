<?php
get_header();
?>
<div class="color-calander">
	<div class="clearfix">&nbsp</div>
	<h2><?php echo 'Title Tag and Meta Description Length Tool';?></h2>
	<p class="text-justify">How many characters can I have in my title and meta descripton? How many characters is this!? Save a few seconds and paste your title tag here and check the length of your character string.</p>
	<div class="clearfix">&nbsp</div>
	<br>
	<h4>Enter &lt;title&gt; Tag</h4>
	<p class="text-justify">Best practices for title tag length say that your title should no more than 60-80 characters. An important number to keep in mind is Google's maximum title threshold of around 65 characters. Anything longer and your title starts to get truncated. For example, sledswap.com's title tag runs 159 characters long. <a href="http://s3.ezlocal.com/img/j/2014/1/2014-1-15-407.png" title="Truncated Title Tags" target="_blank">Google SERPs truncate</a> it down to just 53 characters. </p>
	<div class="row">
		<div class="col-md-12">
			<input type="text" id="tTitleTag" placeholder="<title> tag">
			<p id="tTitleTagResults"></p>
		</div>
	</div>
	<div class="clearfix">&nbsp</div>
	<br>
	<br>
	<h4>Enter Meta Description Tag</h4>
	<p>Meta descriptions can be a bit longer, but Google tends to show roughly 155 (two lines) characters when it chooses to use your meta description on result pages. For more, read the <a href="http://moz.com/learn/seo/meta-description" target="_blank">best practices guide by Moz</a>.</p>
	<div class="form-group">
		<label for="meta-title-tag">Meta Title Tag</label>
		<textarea id="tMetaTag" placeholder="meta descrption content"></textarea>
		<p id="tMetaTagResults"></p>
	</div>
	<div class="clearfix">&nbsp</div>
	<br>
	<br>
	<h4>Paste Character Counter</h4>
	<p class="text-justify">Get the length of any string you would like a quick count of characters.</p>
	<div class="row">
		<div class="col-md-12">
		  <textarea class="form-control" rows="5" id="tGeneral"></textarea>
		  <p id="tGeneralResults"></p>
		</div>
	</div>
</div>
<?php
get_footer();
