<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.linkedin.com/in/sahed-sawon-8629a978?trk=hp-identity-name
 * @since      1.0.0
 *
 * @package    Title_tag_and_meta_description_length_tool
 * @subpackage Title_tag_and_meta_description_length_tool/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Title_tag_and_meta_description_length_tool
 * @subpackage Title_tag_and_meta_description_length_tool/includes
 * @author     sahed sawon <smsawoncse@gmail.com>
 */
class Title_tag_and_meta_description_length_tool_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
