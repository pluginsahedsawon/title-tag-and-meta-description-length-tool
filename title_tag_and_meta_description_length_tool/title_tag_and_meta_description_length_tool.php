<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.linkedin.com/in/sahed-sawon-8629a978?trk=hp-identity-name
 * @since             1.0.0
 * @package           Title_tag_and_meta_description_length_tool
 *
 * @wordpress-plugin
 * Plugin Name:       Title_Tag_And_Meta_Description_Length_Tool
 * Plugin URI:        https://wordpress.org/plugins/Title_Tag_And_Meta_Description_Length_Tool
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            sahed sawon
 * Author URI:        https://www.linkedin.com/in/sahed-sawon-8629a978?trk=hp-identity-name
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       title_tag_and_meta_description_length_tool
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-title_tag_and_meta_description_length_tool-activator.php
 */
function activate_title_tag_and_meta_description_length_tool() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-title_tag_and_meta_description_length_tool-activator.php';
	Title_tag_and_meta_description_length_tool_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-title_tag_and_meta_description_length_tool-deactivator.php
 */
function deactivate_title_tag_and_meta_description_length_tool() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-title_tag_and_meta_description_length_tool-deactivator.php';
	Title_tag_and_meta_description_length_tool_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_title_tag_and_meta_description_length_tool' );
register_deactivation_hook( __FILE__, 'deactivate_title_tag_and_meta_description_length_tool' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-title_tag_and_meta_description_length_tool.php';


require_once( plugin_dir_path( __FILE__ ) . 'class-page-template.php' );
add_action( 'plugins_loaded', array( 'Page_Template_Plugin', 'get_instance' ) );
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_title_tag_and_meta_description_length_tool() {

	$plugin = new Title_tag_and_meta_description_length_tool();
	$plugin->run();

}
run_title_tag_and_meta_description_length_tool();
